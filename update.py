"Hey Popo UABE is awesome and I love it and you!"

import json
import os
import shutil
from dataclasses import dataclass
from pathlib import Path
from zipfile import ZipFile

README_TEMPLATE = """# Unofficial UABE Mirror %(version)s

## **WARNING** This mirror is outdated! Get the latest version [here](https://7daystodie.com/forums/showthread.php?22675-Unity-Assets-Bundle-Extractor).

This repository contains a backup of Unity Assets Bundle Extractor (UABE) legacy versions.

UABE is developed by [DerPopo](https://github.com/DerPopo) and released under [CC BY-NC-SA 3.0](/LICENSE.txt).
The changelog can be found [here](/CHANGELOG.md).

- [GUI 32bit](/gui/32bit) / [64bit](/gui/64bit)
- [API](/api)

Some versions and files have been renamed and or moved. Source files are available [here](https://gitlab.com/binkabonka/uabe-mirror-sources).

## Links

- [Official UABE 7 Days to Die Forums Thread](https://7daystodie.com/forums/showthread.php?22675-Unity-Assets-Bundle-Extractor)
- [Official UABE GitHub Repository](https://github.com/DerPopo/UABE)

## Version History

| Date       | Version                   | Downloads
| ---------- | ------------------------- | ---------
%(version_history)s
"""

SOURCES_PATH = Path("../uabe-mirror-sources")

GITLAB_USER = "binkabonka"
GITLAB_REPO = "uabe-mirror"
GITLAB = f"https://gitlab.com/{GITLAB_USER}/{GITLAB_REPO}"


def system(command):
    print(f"> {command}")
    os.system(command)


@dataclass
class Release:
    version: str
    date: str
    zip_files: list()
    actions: dict()
    changelog: list()
    release_type: str = ""
    version_history_row: str = ""

    def __post_init__(self):
        self.release_type = "api" if "_api" in self.version else "gui"
        download_links = ", ".join(
            [
                f"[{ext}]({GITLAB}/-/archive/{self.version}/{GITLAB_REPO}-{self.version}.{ext})"
                for ext in ["zip", "tar.gz", "tar.bz2", "tar"]
            ]
        )
        self.version_history_row = f"| {self.date} | {self.version:<25} | {download_links}"

    def extract(self):
        # git actions (currently only used for mv but can do any git command)
        # this makes git aware of renamed files
        if self.actions and "git" in self.actions:
            for action in self.actions["git"]:
                do = action["do"]
                args = " ".join(action["args"])
                system(f"git {do} {args}")

        # extract
        base_path = Path(".") / self.release_type
        for zip_file in [ZipFile(SOURCES_PATH / "files" / zf) for zf in self.zip_files]:
            for zip_info in [zi for zi in zip_file.filelist if not zi.is_dir()]:
                file_path = base_path / zip_info.filename

                # make sure gui releases go into subfolders
                if self.release_type == "gui":
                    if "32bit" not in zip_info.filename and "64bit" not in zip_info.filename:
                        if "32bit" in zip_file.filename:
                            file_path = base_path / "32bit" / zip_info.filename
                        elif "64bit" in zip_file.filename:
                            file_path = base_path / "64bit" / zip_info.filename

                # unclutter license files
                if "license.txt" in file_path.name or "lgpl.txt" in file_path.name:
                    file_path = Path("./licenses") / file_path.name

                # make parent dirs
                file_path.parent.mkdir(parents=True, exist_ok=True)

                # write file
                file_path.open("wb").write(zip_file.read(zip_info.filename))

        # file actions (copy, move or delete files on disk)
        # this cleans up the extracted files, like deleting the classdata.dat files
        # or moving Usage.txt into the bit folders in early versions
        if self.actions and "file" in self.actions:
            for action in self.actions["file"]:
                do = action["do"]

                if do in ["copy", "move"]:
                    source, destination = [Path(arg) for arg in action["args"]]

                    # make parent dirs
                    destination.parent.mkdir(parents=True, exist_ok=True)

                    # delete if already exists
                    if destination.is_file():
                        destination.unlink()

                    if do == "move":
                        Path(source).rename(destination)
                    elif do == "copy":
                        Path(destination).write_bytes(Path(source).read_bytes())

                elif do == "delete":
                    for file in [Path(arg) for arg in action["args"]]:
                        if file.is_file():
                            file.unlink()


def load_releases():
    releases = list()
    releases_json = json.load((SOURCES_PATH / "releases.json").open())
    for version, release in releases_json.items():
        date = release["date"]
        zip_files = list(release["files"].keys())
        actions = release["actions"] if "actions" in release else dict()
        changelog = release["changelog"] if "changelog" in release else list()
        releases.append(Release(version, date, zip_files, actions, changelog))
    return releases


def commit(message, date=None):
    name = "UABEM"
    email = "UABEM@noreply.com"

    for person in ["AUTHOR", "COMMITTER"]:
        os.environ[f"GIT_{person}_NAME"] = name
        os.environ[f"GIT_{person}_EMAIL"] = email
        if date:
            os.environ[f"GIT_{person}_DATE"] = f"{date}T12:00:00Z"

    system(f"git config user.name {name}")
    system(f"git config user.email {email}")
    system(f'git commit -m "{message}"')


def regenerate():
    for s in [".git", "api", "gui", "licenses", "CHANGELOG.md", "README.md"]:
        path = Path(".") / s
        if path.is_file():
            path.unlink()
        elif path.is_dir():
            shutil.rmtree(path, ignore_errors=True)

    system(f"git init")
    system(f"git remote add origin {GITLAB}")

    update("all")


def update(target="latest"):
    releases = load_releases()
    version_history = list()
    changelog = ["# Changelog"]
    for release in releases:
        version_history.append(release.version_history_row)

        if release.changelog:
            changelog.append(
                f"## {release.version}\n\n- " + "\n- ".join(release.changelog)
            )

        is_target_release = (
            target == release.version
            or target == "all"
            or (target == "latest" and release.version == releases[-1].version)
        )
        if is_target_release:
            # extract zip files
            release.extract()

            # write readme file for this release
            readme = README_TEMPLATE % {
                "version": release.version,
                "version_history": "\n".join(version_history),
            }
            Path("./README.md").write_text(readme)

            # write changelog file for this release
            Path("./CHANGELOG.md").write_text("\n\n".join(changelog) + "\n")

            # git add, commit and tag
            system("git add .")
            commit(release.version, release.date)
            system(f"git tag {release.version}")


def push():
    system("git push -u origin --all")
    system("git push -u origin --tags")


if __name__ == "__main__":
    update("latest")
    push()
